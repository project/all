# All at once configuration

## Installation

To install this module, `composer require drupal/all`, or  place it in your modules
folder and enable it on the modules page.


## Configuration

All settings for this module are nearest to where the configuration would normally be done individually.

So far we have content types on the All content types configuration page, under Administration > Structure > Content types, where this module adds an action link.  You can visit the configuration page directly at admin/config/content/all.


## Credit

The All module was originally and is currently developed by Benjamin Melançon of [Agaric Technology Cooperative](https://agaric.coop/).
