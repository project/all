<?php

namespace Drupal\all\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\comment\Entity\CommentType;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Honeypot module routes.
 */
class AllTypesSettingsController extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * A cache backend interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a settings controller.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, CacheBackendInterface $cache_backend, MessengerInterface $messenger) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->cache = $cache_backend;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('cache.default'),
      $container->get('messenger')
    );
  }

  /**
   * Get a value from the retrieved form settings array.
   */
  public function getFormSettingsValue($form_settings, $form_id) {
    // If there are settings in the array and the form ID already has a setting,
    // return the saved setting for the form ID.
    if (!empty($form_settings) && isset($form_settings[$form_id])) {
      return $form_settings[$form_id];
    }
    // Default to false.
    else {
      return 0;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['all.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'all_types_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['note'] = ['#markup' => '<em>' . $this->t('Apologies that for now you need to know the internal savings meanings.  The config system itself does not know the human-readable versions of its variables, so we have to grab constants from the node module etc.') . '</em>'];

    $allable = [
      'display_submitted' => $this->t('Display author/date'),
      'new_revision' => $this->t('New revision'),
      'preview_mode' => $this->t('Preview'),
      'status' => $this->t('Published'),
      'promote' => $this->t('Promoted'),
      'sticky' => $this->t('Sticky'),
    ];
    // For the annoying ones which are not simple checkboxes:
    $options = [
      'preview_mode' => [
        DRUPAL_DISABLED => $this->t('Disabled'),
        DRUPAL_OPTIONAL => $this->t('Optional'),
        DRUPAL_REQUIRED => $this->t('Required'),
      ],
    ];

    $header = [
      'content_type' => $this->t('Content type'),
    ] + $allable;

    // Most tables a $rows variable is made first and then added to the table
    // in the '#rows' key, but the permissions table isn't done like that, and
    // there must be a reason, because the usual approach isn't working for us
    // so we will follow the permissions table which is frighteningly closest
    // to what we're trying to do.
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
    ];

    // heh we do this in the submit but get node types the API way below.
    // $ctypes = \Drupal::service('config.factory')->listAll('node.type');
    // Node types.
    if ($this->moduleHandler->moduleExists('node')) {
      $types = NodeType::loadMultiple();
      if (!empty($types)) {
        foreach ($types as $id => $type) {
          $form['table'][$id]['content_type'] = [
            '#type' => 'link',
            '#title' => $type->label(),
            '#url' => $type->toUrl(),
            '#localized_options' => '',
          ];
            
          foreach ($allable as $key => $name) {
            $form['table'][$id][$key] = [
              '#type' => 'checkbox',
              '#default_value' => $this->config('node.type.' . $id)->get($key),
            ];
            // Handle our non-checkbox by overriding above; easier than if-else.
            if (isset($options[$key])) {
              $form['table'][$id][$key]['#type'] = 'select';
              $form['table'][$id][$key]['#options'] = $options[$key];
            }
          }
        }
      }
    }

    // For now, manually add submit button. Hopefully, by the time D8 is
    // released, there will be something like system_settings_form() in D7.
    // @TODO it's D9 now.  Check (and share back to honeypot)
    $form['actions']['#type'] = 'container';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // We could try to steal all the validation from the original form piece
    // by piece but this is an admin-only thing and not much you can damage
    // trying to stick `alert='gotcha'` in where a 1 belongs.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $ctypes = \Drupal::service('config.factory')->listAll('node.type');
    $type_values = $form_state->getValue('table');

    // Clear the cache for each node type; undoubtedly more should be done but this is a start.
    foreach ($type_values as $type => $values) {
      $ctype = 'node.type.' . $type;
      // We don't validate but we won't save random config in this form either.
      if (!in_array($ctype, $ctypes)) {
        return;
      }
      // Something very bizarre is happening because $this->config->getEditable
      // results in 'Call to a member function getEditable() on null'
      $config = \Drupal::configFactory()->getEditable($ctype);
      foreach ($values as $key => $value) {
        $config->set($key, $value);
      }
      $config->save();
      $this->cache->delete('config:' . $type);
    }

    // Tell the user the settings have been saved.
    $this->messenger->addMessage($this->t('The configuration options for :count content types have been saved.', [':count' => count($ctypes)]));
  }

}
